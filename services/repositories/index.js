const { Group, User } = require('../db/models')

module.exports = {
  listGroups: () => Group.findAll({
    attributes: ['id', 'title'],
    order: ['title']
  }),
  listGroupsWithUsers: () => Group.findAll({
    attributes: ['id', 'title'],
    order: ['title'],
    include: [
      { model: User, attributes: ['id', 'firstName'] },
    ]
  }),
  listUsers: () => User.findAll({
    attributes: ['id', 'firstName', 'lastName', 'email'],
    order: ['firstName'],
    include: [
      { model: Group, attributes: ['id', 'title'] }
    ]
  }),
  getUser: id => User.findById(id, {
    attributes: ['id', 'firstName', 'lastName', 'email', 'groupId']
  }),
  insertUser: user => User.create(user)
}
