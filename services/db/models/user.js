const bcrypt = require('bcrypt')

module.exports = (sequelize, Sequelize) =>
  sequelize.define('user', {
    firstName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: [3, 255],
          msg: 'O nome deve ter entre 3 e 255 caracteres'
        }
      },
    },
    lastName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: [3, 255],
      },
    },
    email: {
      type: Sequelize.STRING,
      unique: true,
      validate: {
        isEmail: {
          args: true,
          msg: 'Forneça um email válido!'
        },
        notEmpty: true
      },
    },
    senha_limpa: {
      type: Sequelize.VIRTUAL,
      validate: {
        len: {
          args: [6, 20],
          msg: 'A senha deve ter de 6 a 20 caracteres!'
        },
      },
      get() {
        return null
      }
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
      set(val) {
        this.setDataValue('senha_limpa', val)
        this.setDataValue('password', bcrypt.hashSync(val, 12))
      },
      get() {
        return null
      }
    },
    birthday: {
      type: Sequelize.DATEONLY,
      validate: {
        isDate: true,
      },
    },
    host: {
      type: Sequelize.STRING,
      validate: {
        isIP: true,
      },
    },
    phone: Sequelize.STRING,
    lastLogin: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
  })
