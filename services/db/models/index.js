const Sequelize = require('sequelize')

const conn = require('../config')

const sequelize = new Sequelize(conn.base, conn.user, conn.pass, {
  host: conn.host,
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
})

const User = require('./user')(sequelize, Sequelize)
const Group = require('./group')(sequelize, Sequelize)

User.belongsTo(Group)
Group.hasMany(User)

module.exports = {
  Sequelize,
  sequelize,
  User,
  Group,
}
