const express = require('express')
const {
  insertUser,
  listGroups,
  listUsers,
  getUser
} = require('../services/repositories')
const st = require('striptags')

const router = express.Router()

router.get('/', (req, res, next) => {
  listUsers().then((users) => {
    console.log(users)
    res.render('users', { users })
  }).catch((err) => {
    next(err)
  })
})

router.get('/cadastro', (req, res) => {
  listGroups().then((groups) => {
    res.render('form_cadastro', { groups })
  })
})

router.post('/', (req, res) => {
  insertUser(req.body)
    .then(() => {
      res.redirect('/usuarios')
    })
    .catch((err) => {
      listGroups().then((groups) => {
        req.body.groups = groups
        req.body.errors = err.errors
        res.render('form_cadastro', req.body)
      })
    })
})

router.get('/:id([0-9]{1,4})', (req, res, next) => {
  listGroups().then(groups =>
    getUser(req.params.id).then((user) => {
      if (user) {
        res.render('user', { user, groups })
        return
      }
      throw new Error('Usuário não encontrado')
    }))
    .catch((err) => {
      next(err)
    })
})

router.post('/:id([0-9]{1,4})', (req, res, next) => {
  console.log(req.ip)
  if (req.body.method) {
    req.method = req.body.method.toUpperCase()
  }
  next()
})

router.delete('/:id([0-9]{1,4})', (req, res, next) => {
  getUser(req.params.id)
    .then((user) => {
      if (user) {
        return user.destroy()
      }
      res.status(404).render('error', { message: 'Usuário não encontrado' })
    }).then(() => {
      res.redirect('/usuarios')
      next()
    }).catch((err) => {
      next(err)
    })
})

router.patch('/:id([0-9]{1,4})', (req, res, next) => {
  getUser(req.params.id)
    .then((user) => {
      if (user) {
        user.firstName = st(req.body.firstName)
        user.lastName = st(req.body.lastName)
        user.email = req.body.email
        user.groupId = req.body.groupId
        user.save()
          .then(() => {
            res.redirect('/usuarios')
            next()
          })
          .catch((err) => {
            console.log(err.errors)
            const fields = err.errors.map(item => item.path)
            res.render('user', { user, errors: err.errors, fields })
          })
        return
      }
      throw new Error('Usuário inexistente')
    }).catch((err) => {
      next(err)
    })
})

module.exports = router
