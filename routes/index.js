const express = require('express')
const users = require('./users')
const groups = require('./groups')

const router = express.Router()

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index')
})

router.get('/login', (req, res) => {
  res.render('login')
})

router.post('/login', (req, res) => {
  res.send(req.body)
})

router.use('/usuarios', users)
router.use('/grupos', groups)

module.exports = router
