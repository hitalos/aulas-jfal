const router = require('express').Router()
const { listGroupsWithUsers } = require('../services/repositories')

router.get('/', (req, res) => {
  listGroupsWithUsers().then((groups) => {
    res.render('groups', { groups })
  })
})

module.exports = router
