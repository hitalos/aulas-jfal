const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const express = require('express')
const helmet = require('helmet')
const logger = require('morgan')
const path = require('path')

const index = require('./routes')

const app = express()

app.set('view engine', 'pug')
app.locals.pretty = true
app.use(helmet())

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', index)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  const { message } = err
  const error = process.env.NODE_ENV === 'dev' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error', { message, error })
})

module.exports = app
